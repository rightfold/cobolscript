import ast = require("./ast");

export class LexError extends Error {
    constructor(message: string) {
        super();
        this.message = message;
    }
}

export class EOFError extends LexError {
    constructor() {
        super("unexpected EOF");
    }
}

export enum Token {
    ALPHANUMERIC_LITERAL,
    FIXED_POINT_LITERAL,

    PERIOD,

    IDENTIFIER,

    ACCEPT_KEYWORD,
    ADD_KEYWORD,
    ADVANCING_KEYWORD,
    BY_KEYWORD,
    CALL_KEYWORD,
    DATA_KEYWORD,
    DISPLAY_KEYWORD,
    DIVIDE_KEYWORD,
    DIVISION_KEYWORD,
    ELSE_KEYWORD,
    END_IF_KEYWORD,
    EQUAL_KEYWORD,
    FROM_KEYWORD,
    GIVING_KEYWORD,
    GOBACK_KEYWORD,
    IDENTIFICATION_KEYWORD,
    IF_KEYWORD,
    IS_KEYWORD,
    LINKAGE_KEYWORD,
    MULTIPLY_KEYWORD,
    NO_KEYWORD,
    PICTURE_KEYWORD,
    PROCEDURE_KEYWORD,
    PROGRAM_ID_KEYWORD,
    REFERENCE_KEYWORD,
    SECTION_KEYWORD,
    SUBTRACT_KEYWORD,
    THEN_KEYWORD,
    TO_KEYWORD,
    USING_KEYWORD,
    VALUE_KEYWORD,
    WITH_KEYWORD,
    WORKING_STORAGE_KEYWORD,
}

export interface Lexeme {
    token: Token,
    value: any,
}

export class Lexer {
    private _text: string;

    private set text(value: string) {
        this._text = value;
    }

    private get text(): string {
        if (this._text.length === 0) {
            throw new EOFError();
        }
        return this._text;
    }

    constructor(text: string) {
        text = discardCarriageReturns(text);
        text = discardSequenceNumberArea(text);
        text = discardComments(text);
        text = discardIndicators(text);
        this._text = text;
    }

    get save(): Lexer {
        const copy = new Lexer("");
        copy._text = this._text;
        return copy;
    }

    load(from: Lexer): void {
        this._text = from._text;
    }

    readLexeme(): Lexeme {
        while (isWhitespace(this.text[0])) {
            this.text = this.text.slice(1);
        }

        if (isIdentifierHead(this.text[0])) {
            let identifier = "";
            do {
                identifier += this.text[0];
                this.text = this.text.slice(1);
            } while (isIdentifierTail(this.text[0]));
            const keyword = this.keywords[identifier.toUpperCase()];
            if (keyword === undefined) {
                return { token: Token.IDENTIFIER, value: identifier };
            } else {
                return { token: keyword, value: null };
            }
        }

        if (isDigit(this.text[0])) {
            let value = "";
            do {
                value += this.text[0];
                this.text = this.text.slice(1);
            } while (isDigit(this.text[0]));
            return { token: Token.FIXED_POINT_LITERAL, value: parseInt(value) };
        }

        if (this.text[0] === ".") {
            this.text = this.text.slice(1);
            return { token: Token.PERIOD, value: null };
        }

        if (this.text[0] === "\"") {
            this.text = this.text.slice(1);
            let value = "";
            while (this.text[0] !== "\"") {
                value += this.text[0];
                this.text = this.text.slice(1);
            }
            this.text = this.text.slice(1);
            return { token: Token.ALPHANUMERIC_LITERAL, value: value };
        }

        throw new LexError("unexpected character");
    }

    readPicture(): ast.Picture {
        const regexp = /^[ \n]*(X|9)\((\d+)\)/;
        const match = regexp.exec(this.text);
        if (match === null) {
            throw new LexError("expected picture");
        }
        this.text = this.text.slice(match[0].length);
        const count = parseInt(match[2]);
        switch (match[1]) {
            case "X": return new ast.Picture([new ast.AlphanumericPictureElement(count)]);
            case "9": return new ast.Picture([new ast.NumericPictureElement(count)]);
        }
    }

    private get keywords(): { [spelling: string]: Token } {
        return {
            "ACCEPT": Token.ACCEPT_KEYWORD,
            "ADD": Token.ADD_KEYWORD,
            "ADVANCING": Token.ADVANCING_KEYWORD,
            "BY": Token.BY_KEYWORD,
            "CALL": Token.CALL_KEYWORD,
            "DATA": Token.DATA_KEYWORD,
            "DISPLAY": Token.DISPLAY_KEYWORD,
            "DIVIDE": Token.DIVIDE_KEYWORD,
            "DIVISION": Token.DIVISION_KEYWORD,
            "ELSE": Token.ELSE_KEYWORD,
            "END-IF": Token.END_IF_KEYWORD,
            "EQUAL": Token.EQUAL_KEYWORD,
            "FROM": Token.FROM_KEYWORD,
            "GIVING": Token.GIVING_KEYWORD,
            "GOBACK": Token.GOBACK_KEYWORD,
            "IDENTIFICATION": Token.IDENTIFICATION_KEYWORD,
            "IF": Token.IF_KEYWORD,
            "IS": Token.IS_KEYWORD,
            "LINKAGE": Token.LINKAGE_KEYWORD,
            "MULTIPLY": Token.MULTIPLY_KEYWORD,
            "NO": Token.NO_KEYWORD,
            "PIC": Token.PICTURE_KEYWORD,
            "PICTURE": Token.PICTURE_KEYWORD,
            "PROCEDURE": Token.PROCEDURE_KEYWORD,
            "PROGRAM-ID": Token.PROGRAM_ID_KEYWORD,
            "REFERENCE": Token.REFERENCE_KEYWORD,
            "SECTION": Token.SECTION_KEYWORD,
            "SUBTRACT": Token.SUBTRACT_KEYWORD,
            "THEN": Token.THEN_KEYWORD,
            "TO": Token.TO_KEYWORD,
            "USING": Token.USING_KEYWORD,
            "VALUE": Token.VALUE_KEYWORD,
            "WITH": Token.WITH_KEYWORD,
            "WORKING-STORAGE": Token.WORKING_STORAGE_KEYWORD,
        };
    }
}

function isWhitespace(char: string): boolean {
    return " \n".indexOf(char) !== -1;
}

function isIdentifierHead(char: string): boolean {
    return "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".indexOf(char) !== -1;
}

function isIdentifierTail(char: string): boolean {
    return isIdentifierHead(char) || isDigit(char) || char === "-";
}

function isDigit(char: string): boolean {
    return "0123456789".indexOf(char) !== -1;
}

function discardCarriageReturns(text: string): string {
    return text.replace(/\r\n/g, "\n");
}

function discardSequenceNumberArea(text: string): string {
    return text.split("\n").map(l => l.slice(6)).join("\n");
}

function discardComments(text: string): string {
    return text.split("\n").filter(l => l[0] !== "*" && l[0] !== "/").join("\n");
}

function discardIndicators(text: string): string {
    return text
        .split("\n")
        .map(l => {
            switch (l[0]) {
                case undefined: return "";
                case "-": return l.slice(1);
                case " ": return l.slice(1) + "\n";
                default: throw new LexError("bad indicator '" + l[0] + "'");
            }
        })
        .join("");
}
