function indent(text) {
    return text.toString()
        .split("\n")
        .map(l => ("    " + l).replace(/ +$/, ""))
        .join("\n");
}

export interface CompilationUnit { }

export class ProgramDefinition implements CompilationUnit {
    constructor(public programID: ProgramID,
                public workingStorageSection: DataStorageSection,
                public linkageSection: DataStorageSection,
                public parameters: Parameter[],
                public procedureSections: ProcedureSection[]) { }

    toString(): string {
        let result = "";
        result += "IDENTIFICATION DIVISION.\n";
        result += this.programID;

        if (this.workingStorageSection.dataDescriptionEntries.length) {
            result += "DATA DIVISION.\n";
            result += "WORKING-STORAGE SECTION.\n";
            result += this.workingStorageSection;
        }

        result += "PROCEDURE DIVISION";
        if (this.parameters.length) {
            result += " USING " + this.parameters.join(" ");
        }
        result += ".\n";
        for (let procedureSection of this.procedureSections) {
            result += procedureSection;
        }
        return result;
    }
}

export class ProgramID {
    constructor(public name: string,
                public nameAs: string) { }

    toString(): string {
        return "PROGRAM-ID. " + this.name + " AS \"" + this.nameAs + "\".\n";
    }
}

export class DataStorageSection {
    constructor(public dataDescriptionEntries: DataDescriptionEntry[]) { }

    toString(): string {
        return this.dataDescriptionEntries.join("");
    }
}

export class DataDescriptionEntry {
    constructor(public name: string,
                public picture: Picture) { }

    toString(): string {
        let result = "";
        result += "01 ";
        if (this.name === null) {
            result += "FILLER";
        } else {
            result += this.name;
        }
        result += " " + this.picture;
        result += ".\n";
        return result;
    }
}

export class Picture {
    constructor(public elements: PictureElement[]) { }

    toString(): string {
        return "PICTURE IS " + this.elements.join("");
    }
}

export class PictureElement { }

export class AlphanumericPictureElement extends PictureElement {
    constructor(public count: number) {
        super();
    }

    toString(): string {
        return "X" + (this.count === 0 ? "" : "(" + this.count + ")");
    }
}

export class NumericPictureElement extends PictureElement {
    constructor(public count: number) {
        super();
    }

    toString(): string {
        return "9" + (this.count === 0 ? "" : "(" + this.count + ")");
    }
}

export class Parameter {
    constructor(public name: string) { }
}

export class ByValueParameter extends Parameter {
    toString(): string {
        return "BY VALUE " + this.name;
    }
}

export class ByReferenceParameter extends Parameter {
    toString(): string {
        return "BY REFERENCE " + this.name;
    }
}

export class ProcedureSection {
    constructor(public name: string,
                public paragraphs: ProcedureParagraph[]) { }

    toString(): string {
        let result = "";
        if (this.name !== null) {
            result += name + " SECTION.\n";
        }
        for (let paragraph of this.paragraphs) {
            result += paragraph;
        }
        return result;
    }
}

export class ProcedureParagraph {
    constructor(public name: string, public sentences: Sentence[]) { }

    toString(): string {
        let result = "";
        if (this.name !== null) {
            result += name + ".\n";
        }
        for (let sentence of this.sentences) {
            result += indent(sentence);
        }
        return result;
    }
}

export class Sentence {
    constructor(public statements: Statement[]) { }

    toString(): string {
        let result = "";
        for (let statement of this.statements) {
            result += statement;
        }
        result += ".\n";
        return result;
    }
}

export class Statement { }

export class ArithmeticStatement extends Statement {
    constructor(public operandA: Identifier,
                public operandB: Identifier,
                public giving: Identifier) {
        super();
    }

    toString(): string {
        return this.kind + " " + this.operandA
             + " " + this.to + " " + this.operandB
             + " GIVING " + this.giving
             + "\n";
    }

    get kind(): string { throw "override"; }
    get to(): string { throw "override"; }
}

export class AddStatement extends ArithmeticStatement {
    get kind(): string { return "ADD"; }
    get to(): string { return "TO"; }
}

export class AcceptStatement extends Statement {
    constructor(public operand: Identifier) {
        super();
    }

    toString(): string {
        return "ACCEPT " + this.operand + "\n";
    }
}

export class CallStatement extends Statement {
    arguments: Argument[];

    constructor(public callee: Identifier,
                args: Argument[]) {
        super();
        this.arguments = args;
    }

    toString(): string {
        let result = "";
        result += "CALL " + this.callee;
        if (this.arguments.length) {
            result += " USING " + this.arguments.join(" ");
        }
        result += "\n";
        return result;
    }
}

export class Argument {
    constructor(public value: Identifier) { }
}

export class ByValueArgument extends Argument {
    toString(): string {
        return "BY VALUE " + this.value;
    }
}

export class ByReferenceArgument extends Argument {
    toString(): string {
        return "BY REFERENCE " + this.value;
    }
}

export class ContinueStatement extends Statement {
    toString(): string {
        return "CONTINUE\n";
    }
}

export class DisplayStatement extends Statement {
    constructor(public operands: (Identifier | Literal)[],
                public advance: boolean) {
        super();
    }

    toString(): string {
        let result = "";
        result += "DISPLAY " + this.operands.join(" ");
        if (!this.advance) {
            result += " WITH NO ADVANCING";
        }
        result += "\n";
        return result;
    }
}

export class DivideStatement extends ArithmeticStatement {
    get kind(): string { return "DIVIDE"; }
    get to(): string { return "BY"; }
}

export class GobackStatement extends Statement {
    toString(): string {
        return "GOBACK\n";
    }
}

export class IfStatement extends Statement {
    condition: Condition;
    then: Statement[];
    else: Statement[];

    constructor(condition: Condition,
                then: Statement[],
                else_: Statement[]) {
        super();
        this.condition = condition;
        this.then = then;
        this.else = else_;
    }

    toString(): string {
        let result = "";
        result += "IF " + this.condition + " THEN\n";
        result += indent(this.then);
        result += "ELSE\n";
        result += indent(this.else);
        result += "END-IF\n";
        return result;
    }
}

export class MultiplyStatement extends ArithmeticStatement {
    get kind(): string { return "MULTIPLY"; }
    get to(): string { return "BY"; }
}

export class SubtractStatement extends ArithmeticStatement {
    get kind(): string { return "SUBTRACT"; }
    get to(): string { return "FROM"; }
}

export class Condition { }

export enum RelationOperator {
    IS_EQUAL_TO,
}

export class RelationCondition extends Condition {
    constructor(public operator: RelationOperator,
                public left: Identifier | Literal,
                public right: Identifier | Literal) {
        super();
    }

    toString(): string {
        return this.left + " IS EQUAL TO " + this.right;
    }
}

export class Identifier {
    constructor(public name: string) { }

    toString(): string {
        return this.name;
    }
}

export class Literal { }

export class AlphanumericLiteral extends Literal {
    constructor(public value: string) {
        super();
    }

    toString(): string {
        return "\"" + this.value + "\"";
    }
}

export class FixedPointLiteral extends Literal {
    constructor(public value: number) {
        super();
    }
}
