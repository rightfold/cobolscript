import ast = require("./ast");
import esprima = require("esprima");

let lastID = 0;
function fresh(prefix: string): string {
    return prefix + "_" + ++lastID;
}

class Scope {
    parent: Scope;
    symbols: { [name: string]: Symbol };

    constructor(parent: Scope) {
        this.parent = parent;
        this.symbols = Object.create(null);
    }

    get(name: string) {
        if (name in this.symbols) {
            return this.symbols[name];
        } else if (this.parent !== null) {
            return this.parent.get(name);
        } else {
            throw new Error("name not in scope");
        }
    }

    set(name: string, symbol: Symbol) {
        if (name in this.symbols) {
            throw new Error("redefinition");
        } else {
            this.symbols[name] = symbol;
        }
    }
}

class Symbol { }

class WorkingStorageDataItemSymbol extends Symbol {
    constructor(public programName: string,
                public name: string,
                public picture: ast.Picture) {
        super();
    }
}

class ByValueLinkageDataItemSymbol extends Symbol {
    constructor(public name: string,
                public picture: ast.Picture) {
        super();
    }
}

class ByReferenceLinkageDataItemSymbol extends Symbol {
    constructor(public name: string,
                public picture: ast.Picture) {
        super();
    }
}

function esIdentifier(name: string): ESTree.Identifier {
    return {
        type: esprima.Syntax.Identifier,
        name: name,
    };
}

function esLiteral(value: boolean | number | string): ESTree.Literal {
    return {
        type: esprima.Syntax.Literal,
        value: value,
    };
}

function esPath(name: string, ...names: string[]): ESTree.Expression {
    return names.reduce((all, name) => {
        return <ESTree.MemberExpression>{
            type: esprima.Syntax.MemberExpression,
            computed: false,
            object: all,
            property: esIdentifier(name),
        };
    }, <ESTree.Expression>esIdentifier(name));
}

export function compile(unit: ast.CompilationUnit): ESTree.Program {
    let statements: ESTree.Statement[];

    if (unit instanceof ast.ProgramDefinition) {
        statements = compileProgram(unit);
    } else {
        throw new Error("unsupported compilation unit");
    }

    return <ESTree.Program>{
        type: esprima.Syntax.Program,
        body: statements,
    };
}

function compileProgram(definition: ast.ProgramDefinition): ESTree.Statement[] {
    const scope = new Scope(null);
    let body = [];

    for (let workingStorageDataDescriptionEntry
            of definition.workingStorageSection.dataDescriptionEntries) {
        const symbol = new WorkingStorageDataItemSymbol(
            definition.programID.nameAs,
            workingStorageDataDescriptionEntry.name,
            workingStorageDataDescriptionEntry.picture
        );
        scope.set(workingStorageDataDescriptionEntry.name, symbol);
    }

    for (let linkageDataDescriptionEntry
            of definition.linkageSection.dataDescriptionEntries) {
        var name = linkageDataDescriptionEntry.name;

        const parameter = definition.parameters.filter(p => p.name === name)[0];
        if (parameter === undefined) {
            continue;
        }

        let convention;
        if (parameter instanceof ast.ByValueParameter) {
            convention = ByValueLinkageDataItemSymbol;
        } else if (parameter instanceof ast.ByReferenceParameter) {
            convention = ByReferenceLinkageDataItemSymbol;
        } else {
            throw new Error("unsupported parameter type");
        }

        const symbol = new convention(name, linkageDataDescriptionEntry.picture);
        scope.set(linkageDataDescriptionEntry.name, symbol);
    }

    for (let procedureSection of definition.procedureSections) {
        for (let paragraph of procedureSection.paragraphs) {
            for (let sentence of paragraph.sentences) {
                for (let statement of sentence.statements) {
                    body = body.concat(compileStatement(statement, scope));
                }
            }
        }
    }

    const statements = [];
    statements.push(<ESTree.FunctionDeclaration>{
        type: esprima.Syntax.FunctionDeclaration,
        id: esIdentifier(definition.programID.nameAs),
        params: definition.parameters.map(u => esIdentifier("linkage_" + u.name)),
        defaults: [],
        rest: null,
        body: <ESTree.BlockStatement>{
            type: esprima.Syntax.BlockStatement,
            body: body,
        },
        generator: false,
        expression: false,
    });
    statements.push(<ESTree.ExpressionStatement>{
        type: esprima.Syntax.ExpressionStatement,
        expression: <ESTree.AssignmentExpression>{
            type: esprima.Syntax.AssignmentExpression,
            operator: "=",
            left: esPath(definition.programID.nameAs, "workingStorage"),
            right: <ESTree.ObjectExpression>{
                type: esprima.Syntax.ObjectExpression,
                properties: [],
            },
        },
    });
    for (let workingStorageDataDescriptionEntry
            of definition.workingStorageSection.dataDescriptionEntries) {
        statements.push(<ESTree.ExpressionStatement>{
            type: esprima.Syntax.ExpressionStatement,
            expression: <ESTree.AssignmentExpression>{
                type: esprima.Syntax.AssignmentExpression,
                operator: "=",
                left: esPath(
                    definition.programID.nameAs,
                    "workingStorage",
                    workingStorageDataDescriptionEntry.name
                ),
                right: esLiteral(""),
            },
        });
    }
    return statements;
}

function compileStatement(statement: ast.Statement, scope: Scope): ESTree.Statement[] {
    if (statement instanceof ast.AcceptStatement) {
        const temporaryStorage = esIdentifier(fresh("accept"));
        const operandSymbol = scope.get(statement.operand.name);
        return [
            <ESTree.VariableDeclaration>{
                type: esprima.Syntax.VariableDeclaration,
                kind: "var",
                declarations: [{
                    type: esprima.Syntax.VariableDeclarator,
                    id: temporaryStorage,
                    init: <ESTree.CallExpression>{
                        type: esprima.Syntax.CallExpression,
                        callee: esPath("cobolscript", "accept"),
                        arguments: [esLiteral(operandSymbol.picture.elements.join(""))],
                    },
                }],
            },
            <ESTree.IfStatement>{
                type: esprima.Syntax.IfStatement,
                test: <ESTree.BinaryExpression>{
                    type: esprima.Syntax.BinaryExpression,
                    operator: "!==",
                    left: temporaryStorage,
                    right: esLiteral(null),
                },
                consequent: <ESTree.ExpressionStatement>{
                    type: esprima.Syntax.ExpressionStatement,
                    expression: <ESTree.AssignmentExpression>{
                        type: esprima.Syntax.AssignmentExpression,
                        operator: "=",
                        left: compileIdentifier(statement.operand, scope),
                        right: temporaryStorage,
                    },
                },
                alternate: null,
            },
        ];
    } else if (statement instanceof ast.ArithmeticStatement) {
        const operator = {
            "ADD": "add",
            "DIVIDE": "divide",
            "MULTIPLY": "multiply",
            "SUBTRACT": "subtract",
        }[statement.kind];
        const givingSymbol = scope.get(statement.giving.name);
        return [<ESTree.ExpressionStatement>{
            type: esprima.Syntax.ExpressionStatement,
            expression: <ESTree.AssignmentExpression>{
                type: esprima.Syntax.AssignmentExpression,
                operator: "=",
                left: compileIdentifier(statement.giving, scope),
                right: <ESTree.CallExpression>{
                    type: esprima.Syntax.CallExpression,
                    callee: esPath("cobolscript", operator),
                    arguments: [
                        compileIdentifier(statement.operandA, scope),
                        compileIdentifier(statement.operandB, scope),
                        esLiteral(givingSymbol.picture.elements.join(""))
                    ],
                },
            },
        }];
    } else if (statement instanceof ast.CallStatement) {
        let statements = [];
        let arguments_: ESTree.Expression[] = [];
        let argumentCells: [ESTree.Expression, ESTree.Expression][] = [];
        for (let argument of statement.arguments) {
            if (argument instanceof ast.ByValueArgument) {
                arguments_.push(compileIdentifier(argument.value, scope));
            } else if (argument instanceof ast.ByReferenceArgument) {
                const argumentValue = compileIdentifier(argument.value, scope);
                const argumentCell = esIdentifier(fresh("cell"));
                argumentCells.push([argumentValue, argumentCell]);
                arguments_.push(argumentCell)
                statements.push(<ESTree.VariableDeclaration>{
                    type: esprima.Syntax.VariableDeclaration,
                    kind: "var",
                    declarations: [<ESTree.VariableDeclarator>{
                        type: esprima.Syntax.VariableDeclarator,
                        id: argumentCell,
                        init: <ESTree.NewExpression>{
                            type: esprima.Syntax.NewExpression,
                            callee: esPath("cobolscript", "Cell"),
                            arguments: [argumentValue],
                        },
                    }],
                });
            } else {
                throw new Error("unsupported argument");
            }
        }
        statements.push(<ESTree.ExpressionStatement>{
            type: esprima.Syntax.ExpressionStatement,
            expression: <ESTree.CallExpression>{
                type: esprima.Syntax.CallExpression,
                callee: esIdentifier(statement.callee.name),
                arguments: arguments_,
            },
        });
        for (let [to, from] of argumentCells) {
            statements.push(<ESTree.ExpressionStatement>{
                type: esprima.Syntax.ExpressionStatement,
                expression: <ESTree.AssignmentExpression>{
                    type: esprima.Syntax.AssignmentExpression,
                    operator: "=",
                    left: to,
                    right: <ESTree.MemberExpression>{
                        type: esprima.Syntax.MemberExpression,
                        computed: false,
                        object: from,
                        property: esIdentifier("value"),
                    },
                },
            });
        }
        return statements;
    } else if (statement instanceof ast.DisplayStatement) {
        return [<ESTree.ExpressionStatement>{
            type: esprima.Syntax.ExpressionStatement,
            expression: <ESTree.CallExpression>{
                type: esprima.Syntax.CallExpression,
                callee: esPath("cobolscript", "display"),
                arguments: [
                    esLiteral(statement.advance),
                    ...statement.operands.map(operand => {
                        if (operand instanceof ast.Identifier) {
                            return compileIdentifier(operand, scope);
                        } else if (operand instanceof ast.Literal) {
                            return compileLiteral(operand);
                        } else {
                            throw new Error("unsupported DISPLAY operand");
                        }
                    }),
                ],
            },
        }];
    } else if (statement instanceof ast.GobackStatement) {
        return [<ESTree.ReturnStatement>{
            type: esprima.Syntax.ReturnStatement,
            argument: null,
        }];
    } else if (statement instanceof ast.IfStatement) {
        return [<ESTree.IfStatement>{
            type: esprima.Syntax.IfStatement,
            test: compileCondition(statement.condition, scope),
            consequent: <ESTree.BlockStatement>{
                type: esprima.Syntax.BlockStatement,
                body: [].concat(...statement.then.map(s => compileStatement(s, scope))),
            },
            alternate: <ESTree.BlockStatement>{
                type: esprima.Syntax.BlockStatement,
                body: [].concat(...statement.else.map(s => compileStatement(s, scope))),
            },
        }];
    } else {
        throw new Error("unsupported statement");
    }
}

function compileCondition(condition: ast.Condition, scope: Scope): ESTree.Expression {
    if (condition instanceof ast.RelationCondition) {
        const compileOperand = function(operand: ast.Identifier | ast.Literal) {
            if (operand instanceof ast.Identifier) {
                return compileIdentifier(operand, scope);
            } else if (operand instanceof ast.Literal) {
                return compileLiteral(operand);
            } else {
                throw new Error("unsupported operand");
            }
        };
        return <ESTree.CallExpression>{
            type: esprima.Syntax.CallExpression,
            callee: esPath("cobolscript", "isEqualTo"),
            arguments: [
                compileOperand(condition.left),
                compileOperand(condition.right),
            ],
        };
    } else {
        throw new Error("unsupported condition");
    }
}

function compileIdentifier(identifier: ast.Identifier, scope: Scope): ESTree.Expression {
    const symbol = scope.get(identifier.name);
    if (symbol instanceof WorkingStorageDataItemSymbol) {
        return esPath(symbol.programName, "workingStorage", symbol.name);
    } else if (symbol instanceof ByValueLinkageDataItemSymbol) {
        return esIdentifier("linkage_" + symbol.name);
    } else if (symbol instanceof ByReferenceLinkageDataItemSymbol) {
        return esPath("linkage_" + symbol.name, "value");
    } else {
        throw new Error("unsupported symbol");
    }
}

function compileLiteral(literal: ast.Literal): ESTree.Expression {
    if (literal instanceof ast.AlphanumericLiteral) {
        return esLiteral(literal.value);
    } else if (literal instanceof ast.FixedPointLiteral) {
        return esLiteral(literal.value);
    } else {
        throw new Error("unsupported literal");
    }
}
