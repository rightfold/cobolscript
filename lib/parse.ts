import ast = require("./ast");
import lex = require("./lex");

function expect(lexer: lex.Lexer, token: lex.Token): any {
    const lexeme = lexer.readLexeme();
    if (lexeme.token !== token) {
        throw new ParseError("expected " + lex.Token[token] + " but found " + lex.Token[lexeme.token]);
    }
    return lexeme.value;
}

function option<T>(lexer: lex.Lexer, parser: (lexer: lex.Lexer) => T): T {
    const backtrack = lexer.save;
    try {
        return parser(lexer);
    } catch (e) {
        if (e instanceof lex.LexError || e instanceof ParseError) {
            lexer.load(backtrack);
            return null;
        }
        throw e;
    }
}

function many<T>(lexer: lex.Lexer, parser: (lexer: lex.Lexer) => T): T[] {
    let result = [];
    for (;;) {
        const element = option(lexer, parser);
        if (element === null) {
            return result;
        } else {
            result.push(element);
        }
    }
}

function many1<T>(lexer: lex.Lexer, parser: (lexer: lex.Lexer) => T): T[] {
    return [parser(lexer), ...many(lexer, parser)];
}

function oneOf<T>(lexer: lex.Lexer, ...parsers: ((lexer: lex.Lexer) => T)[]): T {
    const backtrack = lexer.save;
    for (let parser of parsers) {
        try {
            return parser(lexer);
        } catch (e) {
            if (e instanceof lex.LexError || e instanceof ParseError) {
                lexer.load(backtrack);
                continue;
            }
            throw e;
        }
    }
    throw new ParseError("oops");
}

function literal(lexer: lex.Lexer): ast.Literal {
    return oneOf<ast.Literal>(lexer, alphanumericLiteral, fixedPointLiteral);
}

function alphanumericLiteral(lexer: lex.Lexer): ast.AlphanumericLiteral {
    const value = expect(lexer, lex.Token.ALPHANUMERIC_LITERAL);
    return new ast.AlphanumericLiteral(value);
}

function fixedPointLiteral(lexer: lex.Lexer): ast.FixedPointLiteral {
    const value = expect(lexer, lex.Token.FIXED_POINT_LITERAL);
    return new ast.FixedPointLiteral(value);
}

function identifier(lexer: lex.Lexer): ast.Identifier {
    const name = expect(lexer, lex.Token.IDENTIFIER);
    return new ast.Identifier(name);
}

export class ParseError extends Error {
    constructor(message: string) {
        super();
        this.message = message;
    }
}

export function parse(lexer: lex.Lexer): ast.CompilationUnit {
    const compilationUnit = parseCompilationUnit(lexer);
    try {
        lexer.readLexeme();
    } catch (e) {
        if (e instanceof lex.EOFError) {
            return compilationUnit;
        }
        throw e;
    }
    throw new ParseError("expected EOF");
}

function parseCompilationUnit(lexer: lex.Lexer): ast.CompilationUnit {
    return parseProgramDefinition(lexer);
}

function parseProgramDefinition(lexer: lex.Lexer): ast.ProgramDefinition {
    option(lexer, lexer => {
        expect(lexer, lex.Token.IDENTIFICATION_KEYWORD);
        expect(lexer, lex.Token.DIVISION_KEYWORD);
        expect(lexer, lex.Token.PERIOD);
    });

    const programID = parseProgramID(lexer);

    let workingStorageSection = new ast.DataStorageSection([]);
    let linkageSection = new ast.DataStorageSection([]);
    option(lexer, lexer => {
        expect(lexer, lex.Token.DATA_KEYWORD);
        expect(lexer, lex.Token.DIVISION_KEYWORD);
        expect(lexer, lex.Token.PERIOD);

        for (let {section, keyword} of [
            {section: workingStorageSection, keyword: lex.Token.WORKING_STORAGE_KEYWORD},
            {section: linkageSection, keyword: lex.Token.LINKAGE_KEYWORD},
        ]) {
            option(lexer, ((section, keyword, lexer) => {
                expect(lexer, keyword);
                expect(lexer, lex.Token.SECTION_KEYWORD);
                expect(lexer, lex.Token.PERIOD);

                section.dataDescriptionEntries =
                    many(lexer, parseDataDescriptionEntry);
            }).bind(null, section, keyword));
        }
    });

    expect(lexer, lex.Token.PROCEDURE_KEYWORD);
    expect(lexer, lex.Token.DIVISION_KEYWORD);
    let parameters = option(lexer, lexer => {
        expect(lexer, lex.Token.USING_KEYWORD);
        return many1(lexer, lexer => {
            expect(lexer, lex.Token.BY_KEYWORD);
            const convention = oneOf(
                lexer,
                lexer => (expect(lexer, lex.Token.VALUE_KEYWORD), ast.ByValueParameter),
                lexer => (expect(lexer, lex.Token.REFERENCE_KEYWORD), ast.ByReferenceParameter)
            );
            const name = expect(lexer, lex.Token.IDENTIFIER);
            return new convention(name);
        });
    }) || [];
    expect(lexer, lex.Token.PERIOD);

    let hadFirstProcedureSection = 0;
    const procedureSections =
        many(lexer, l => parseProcedureSection(l, !(hadFirstProcedureSection++)));

    return new ast.ProgramDefinition(
        programID,
        workingStorageSection,
        linkageSection,
        parameters,
        procedureSections
    );
}

function parseProgramID(lexer: lex.Lexer): ast.ProgramID {
    expect(lexer, lex.Token.PROGRAM_ID_KEYWORD);
    expect(lexer, lex.Token.PERIOD);
    const name = <string>expect(lexer, lex.Token.IDENTIFIER);
    expect(lexer, lex.Token.PERIOD);
    return new ast.ProgramID(name, name);
}

function parseDataDescriptionEntry(lexer: lex.Lexer): ast.DataDescriptionEntry {
    expect(lexer, lex.Token.FIXED_POINT_LITERAL);
    const name = expect(lexer, lex.Token.IDENTIFIER);
    const picture = parsePictureClause(lexer);
    expect(lexer, lex.Token.PERIOD);
    return new ast.DataDescriptionEntry(name, picture);
}

function parsePictureClause(lexer: lex.Lexer): ast.Picture {
    expect(lexer, lex.Token.PICTURE_KEYWORD);
    option(lexer, lexer => expect(lexer, lex.Token.IS_KEYWORD));
    const picture = lexer.readPicture();
    return picture;
}

function parseProcedureSection(lexer: lex.Lexer, isFirstSection: boolean): ast.ProcedureSection {
    let sectionName = null;
    (p => isFirstSection ? option(lexer, p) : p(lexer))(lexer => {
        sectionName = expect(lexer, lex.Token.IDENTIFIER);
        expect(lexer, lex.Token.SECTION_KEYWORD);
        expect(lexer, lex.Token.PERIOD);
    });

    let hadFirstParagraph = 0;
    const paragraphs = many(lexer, l => parseProcedureParagraph(l, !(hadFirstParagraph++)));

    return new ast.ProcedureSection(sectionName, paragraphs);
}

function parseProcedureParagraph(lexer: lex.Lexer, isFirstParagraph: boolean): ast.ProcedureParagraph {
    let paragraphName = null;
    (p => isFirstParagraph ? option(lexer, p) : p(lexer))(lexer => {
        paragraphName = expect(lexer, lex.Token.IDENTIFIER);
        expect(lexer, lex.Token.PERIOD);
    });
    const sentences = (paragraphName === null ? many1 : many)(lexer, parseSentence);
    return new ast.ProcedureParagraph(paragraphName, sentences);
}

function parseSentence(lexer: lex.Lexer): ast.Sentence {
    const statements = many(lexer, parseStatement);
    expect(lexer, lex.Token.PERIOD);
    return new ast.Sentence(statements);
}

function parseStatement(lexer: lex.Lexer): ast.Statement {
    const token = lexer.save.readLexeme().token;
    switch (token) {
        case lex.Token.ACCEPT_KEYWORD: return parseAcceptStatement(lexer);
        case lex.Token.ADD_KEYWORD: return parseAddStatement(lexer);
        case lex.Token.CALL_KEYWORD: return parseCallStatement(lexer);
        case lex.Token.DISPLAY_KEYWORD: return parseDisplayStatement(lexer);
        case lex.Token.DIVIDE_KEYWORD: return parseDivideStatement(lexer);
        case lex.Token.GOBACK_KEYWORD: return parseGobackStatement(lexer);
        case lex.Token.IF_KEYWORD: return parseIfStatement(lexer);
        case lex.Token.MULTIPLY_KEYWORD: return parseMultiplyStatement(lexer);
        case lex.Token.SUBTRACT_KEYWORD: return parseSubtractStatement(lexer);
        default: throw new ParseError("expected statement but found " + lex.Token[token]);
    }
}

function parseAcceptStatement(lexer: lex.Lexer): ast.AcceptStatement {
    expect(lexer, lex.Token.ACCEPT_KEYWORD);
    const operand = identifier(lexer);
    return new ast.AcceptStatement(operand);
}

function parseAddStatement(lexer: lex.Lexer): ast.AddStatement {
    expect(lexer, lex.Token.ADD_KEYWORD);
    const operandA = identifier(lexer);
    expect(lexer, lex.Token.TO_KEYWORD);
    const operandB = identifier(lexer);
    expect(lexer, lex.Token.GIVING_KEYWORD);
    const giving = identifier(lexer);
    return new ast.AddStatement(operandA, operandB, giving);
}

function parseCallStatement(lexer: lex.Lexer): ast.CallStatement {
    expect(lexer, lex.Token.CALL_KEYWORD);
    const callee = identifier(lexer);
    let arguments_ = option(lexer, lexer => {
        expect(lexer, lex.Token.USING_KEYWORD);
        return many1(lexer, lexer => {
            expect(lexer, lex.Token.BY_KEYWORD);
            const convention = oneOf(
                lexer,
                lexer => (expect(lexer, lex.Token.VALUE_KEYWORD), ast.ByValueArgument),
                lexer => (expect(lexer, lex.Token.REFERENCE_KEYWORD), ast.ByReferenceArgument)
            );
            return new convention(identifier(lexer));
        });
    }) || [];
    return new ast.CallStatement(callee, arguments_);
}

function parseDisplayStatement(lexer: lex.Lexer): ast.DisplayStatement {
    expect(lexer, lex.Token.DISPLAY_KEYWORD);
    const operands = many1(lexer, lexer => oneOf<ast.AlphanumericLiteral | ast.Identifier>(
        lexer,
        identifier,
        alphanumericLiteral
    ));
    const advance = !option(lexer, lexer => {
        expect(lexer, lex.Token.WITH_KEYWORD);
        expect(lexer, lex.Token.NO_KEYWORD);
        expect(lexer, lex.Token.ADVANCING_KEYWORD);
        return true;
    });
    return new ast.DisplayStatement(operands, advance);
}

function parseDivideStatement(lexer: lex.Lexer): ast.AddStatement {
    expect(lexer, lex.Token.DIVIDE_KEYWORD);
    const operandA = identifier(lexer);
    expect(lexer, lex.Token.BY_KEYWORD);
    const operandB = identifier(lexer);
    expect(lexer, lex.Token.GIVING_KEYWORD);
    const giving = identifier(lexer);
    return new ast.DivideStatement(operandA, operandB, giving);
}

function parseGobackStatement(lexer: lex.Lexer): ast.GobackStatement {
    expect(lexer, lex.Token.GOBACK_KEYWORD);
    return new ast.GobackStatement();
}

function parseIfStatement(lexer: lex.Lexer): ast.IfStatement {
    expect(lexer, lex.Token.IF_KEYWORD);
    const condition = parseCondition(lexer);
    expect(lexer, lex.Token.THEN_KEYWORD);
    const then = many1(lexer, parseStatement);
    const else_ = option(lexer, lexer => {
        expect(lexer, lex.Token.ELSE_KEYWORD);
        return many1(lexer, parseStatement);
    });
    expect(lexer, lex.Token.END_IF_KEYWORD);
    return new ast.IfStatement(condition, then, else_);
}

function parseMultiplyStatement(lexer: lex.Lexer): ast.AddStatement {
    expect(lexer, lex.Token.MULTIPLY_KEYWORD);
    const operandA = identifier(lexer);
    expect(lexer, lex.Token.BY_KEYWORD);
    const operandB = identifier(lexer);
    expect(lexer, lex.Token.GIVING_KEYWORD);
    const giving = identifier(lexer);
    return new ast.MultiplyStatement(operandA, operandB, giving);
}

function parseSubtractStatement(lexer: lex.Lexer): ast.AddStatement {
    expect(lexer, lex.Token.SUBTRACT_KEYWORD);
    const operandA = identifier(lexer);
    expect(lexer, lex.Token.FROM_KEYWORD);
    const operandB = identifier(lexer);
    expect(lexer, lex.Token.GIVING_KEYWORD);
    const giving = identifier(lexer);
    return new ast.SubtractStatement(operandA, operandB, giving);
}

function parseCondition(lexer: lex.Lexer): ast.Condition {
    return parseRelationCondition(lexer);
}

function parseRelationCondition(lexer: lex.Lexer): ast.RelationCondition {
    const operandA = oneOf(lexer, identifier, literal);
    expect(lexer, lex.Token.IS_KEYWORD);
    expect(lexer, lex.Token.EQUAL_KEYWORD);
    expect(lexer, lex.Token.TO_KEYWORD);
    const operandB = oneOf(lexer, identifier, literal);
    return new ast.RelationCondition(ast.RelationOperator.IS_EQUAL_TO, operandA, operandB);
}
