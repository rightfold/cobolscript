module cobolscript {
    export function format(value: any, picture: string): string {
        const match = /^(X|9)\((\d+)\)$/.exec(picture);
        switch (match[1]) {
            case "X":
                const characterCount = parseInt(match[2]);
                return value.toString().slice(0, characterCount);

            case "9":
                const digitCount = parseInt(match[2]);
                const normalized = Math.floor(Math.abs(parseInt(value)));
                return (Array(digitCount).join("0") + normalized).slice(-digitCount);
        }
    }
}
