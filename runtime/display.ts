module cobolscript {
    var displayBuffer = "";

    export function display(advance: boolean, ...operands: any[]): void {
        for (let operand of operands) {
            displayBuffer += operand.toString();
        }
        if (advance) {
            console.log(displayBuffer);
            displayBuffer = "";
        }
    }
}
