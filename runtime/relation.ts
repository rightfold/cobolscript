module cobolscript {
    export function isEqualTo(a: any, b: any): boolean {
        return parseInt(a) === parseInt(b);
    }
}
