module cobolscript {
    export function accept(picture: string): string {
        const answer = prompt();
        return answer === null ? null : format(answer, picture);
    }
}
