module cobolscript {
    export function add(a: string, b: string, picture: string): string {
        return format(parseInt(a) + parseInt(b), picture);
    }

    export function subtract(a: string, b: string, picture: string): string {
        return format(parseInt(a) - parseInt(b), picture);
    }

    export function multiply(a: string, b: string, picture: string): string {
        return format(parseInt(a) * parseInt(b), picture);
    }

    export function divide(a: string, b: string, picture: string): string {
        return format(parseInt(a) / parseInt(b), picture);
    }
}
