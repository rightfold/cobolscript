module cobolscript {
    export class Cell<T> {
        constructor(public value: T) { }
    }
}
