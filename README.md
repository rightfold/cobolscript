# cobolscript

## Language extensions

cobolscript does or will support the following language extensions, which must
be enabled explicitly:

### Handling cancelled prompts

The non-temporal, non-screen `ACCEPT` statement with no device specified calls
`window.prompt`. Because `window.prompt` offers a cancelling feature, but the
`ACCEPT` statement does not, this is provided as a language feature.

#### General format

When this language extension is enabled, the format of non-temporal, non-screen
`ACCEPT` statements becomes:

    'ACCEPT' identifier-1
        [ 'FROM' mnemonic-name-1 ]
        [| 'ON' 'CANCEL' imperative-statement-1
         | 'NOT' 'ON' 'CANCEL' imperative-statement-2
         |]
    [ 'END-ACCEPT' ]

#### General rules

1. When the prompt is cancelled:

   1. If the `ON CANCEL` phrase is specified in the `ACCEPT` statement, control
      is transferred to `imperative-statement-1`. Execution then continues
      according to the rules for each statement specified in
      `imperative-statement-1`. If a procedure branching or conditional
      statement that causes explicit transfer of control is executed, control
      is transferred in accordance with the rules for that statement;
      otherwise, upon completion of the execution of `imperative-statement-1`,
      control is transferred to the end of the `ACCEPT` statement and the
      `NOT ON CANCEL` phrase, if specified, is ignored.
   2. Otherwise, the data item referenced by `identifier-1` is left as if the
      `ACCEPT` statement never occurred.

2. Otherwise:

   1. The rules specified in ISO/IEC 1989:2014 section 14.9.1.3, FORMAT 1 are
      followed.

   2. If the `NOT ON CANCEL` phrase is specified in the `ACCEPT` statement,
      control is transferred to `imperative-statement-2`. Execution then
      continues according to the rules for each statement specified in
      `imperative-statement-2`. If a procedure branching or conditional
      statement that causes explicit transfer of control is executed, control
      is transferred in accordance with the rules for that statement;
      otherwise, upon completion of the execution of `imperative-statement-2`,
      control is transferred to the end of the `ACCEPT` statement and the
      `ON CANCEL` phrase, if specified, is ignored.

#### Example

    ACCEPT name
    ON CANCEL
        DISPLAY "You did not enter a name."
    END-ACCEPT

### Code contracts

New `CONTRACT DIVISION`, with the following optional sections:

- In function, method, and program definitions:

  - `PRECONDITION SECTION`
  - `POSTCONDITION SECTION`

- In class and interface definitions:

  - `INVARIANT SECTION`

New `ASSERT` statement.

TODO: Describe in more detail.
