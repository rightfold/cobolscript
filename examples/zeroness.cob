       IDENTIFICATION DIVISION.
       PROGRAM-ID. zeroness.

       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01 n                             PIC 9(31).

       PROCEDURE DIVISION.
           ACCEPT n
           IF n IS EQUAL TO 0 THEN
               DISPLAY n " is zero"
           ELSE
               DISPLAY n " is non-zero"
           END-IF
           GOBACK
           .
