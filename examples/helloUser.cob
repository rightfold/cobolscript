      *Program that asks for the user's name and then greets them.

       IDENTIFICATION DIVISION.
       PROGRAM-ID. helloUser.

       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01 name                          PIC X(100).

       PROCEDURE DIVISION.
           ACCEPT name
           DISPLAY "Hello, " name "!"
           GOBACK
           .
