       IDENTIFICATION DIVISION.
       PROGRAM-ID. call3.

       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01 n                             PIC 9(31).
       01 m                             PIC 9(31).

       PROCEDURE DIVISION.
           ACCEPT n
           CALL call4 USING BY VALUE n BY REFERENCE m
           DISPLAY m
           GOBACK
           .
