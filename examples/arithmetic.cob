      *Program that asks for two numbers and performs arithmetic on them.

       IDENTIFICATION DIVISION.
       PROGRAM-ID. arithmetic.

       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01 a                             PIC 9(9).
       01 b                             PIC 9(9).
       01 result                        PIC 9(18).

       PROCEDURE DIVISION.
           ACCEPT a
           ACCEPT b

           ADD a TO b GIVING result
           DISPLAY a " + " b " = " result

           SUBTRACT a FROM b GIVING result
           DISPLAY a " - " b " = " result

           MULTIPLY a BY b GIVING result
           DISPLAY a " * " b " = " result

           DIVIDE a BY b GIVING result
           DISPLAY a " / " b " = " result

           GOBACK
           .
