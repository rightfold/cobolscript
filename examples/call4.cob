       IDENTIFICATION DIVISION.
       PROGRAM-ID. call4.

       DATA DIVISION.
       LINKAGE SECTION.
       01 n                             PIC 9(31).
       01 m                             PIC 9(31).

       PROCEDURE DIVISION USING BY VALUE n BY REFERENCE m.
           DISPLAY n
           ACCEPT m
           GOBACK
           .
