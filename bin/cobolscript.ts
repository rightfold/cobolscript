import ast = require("../lib/ast");
import codegen = require("../lib/codegen");
import escodegen = require("escodegen");
import fs = require("fs");
import lex = require("../lib/lex");
import parse = require("../lib/parse");

const cobolSource = fs.readFileSync(process.argv[2], "utf-8");
const lexer = new lex.Lexer(cobolSource);
const cobolAST = parse.parse(lexer);
console.log("/*\n" + cobolAST.toString() + "*/");
const esAST = codegen.compile(cobolAST);
const esSource = escodegen.generate(esAST);
console.log(esSource);
